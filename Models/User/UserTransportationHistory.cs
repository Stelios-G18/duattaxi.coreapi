﻿using DuaTaxi.Common.Types;
using DuaTaxi.CoreApi.Models.TaxiSetPick;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.User
{
    public class UserTransportationHistory
    {
        public string Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string DriverName { get; set; }
        public string DriverId { get; set; }
        public DriverPositionStatus DriverPositionStatus { get; set; }
        public List<Marker> Marker { get; set; }

        public UserTransportationHistory()
        {

        }

    }
}
