﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.TaxiApi
{
    
    public class TaxiDriverActivation : IIdentifiable
    {
        public string Id { get; set; }

        public string CustomerId { get; set; }        

        public double Latitude { get; set; }

        public double Longtitute { get; set; }

        public bool IsActive { get; set; }

        public string ConnectionId { get; set; }

        [JsonConstructor]
        public TaxiDriverActivation(string Id, string customerId, double Latitude, double Longtitute ,string connectionId) { 
            this.Id = Id;
            CustomerId = customerId;
            this.Latitude = Latitude;
            this.Longtitute = Longtitute;
            this.ConnectionId = connectionId;
        }

    }
}
