﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Models.TaxiApi;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CarLicenseController : BaseController
    {

        protected IOptions<RestEaseOptions> RestEaseConfig;
        private ILogger<CarLicenseController> _logger;
        WebApiClient wac;
        public CarLicenseController(ILogger<CarLicenseController> _logger, IOptions<RestEaseOptions> RestEaseConfig, IBusPublisher busPublisher, ITracer tracer, CheckOptions check) : base(busPublisher, tracer, check)
        {
            this._logger = _logger;
            this.RestEaseConfig = RestEaseConfig;
        }


        [Route("Taxi/GetById/{CustomerId}")]
        [HttpGet()]
        public async Task<IActionResult> GetById(string CustomerId)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());

                var data = await wac.LoadAsync<CarLicense>("ActivateDriver/DeActivate", CustomerId);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("Bus/GetById/{CustomerId}")]
        [HttpGet()]
        public async Task<IActionResult> GetByBusId(string CustomerId)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "busapi-service", AccessToken());

                var data = await wac.LoadAsync<CarLicense>("ActivateDriver/DeActivate", CustomerId);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("MiniBus/GetById/{CustomerId}")]
        [HttpGet()]
        public async Task<IActionResult> GetByMiniBusId(string CustomerId)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "minibusapi-service", AccessToken());            

                var data = await wac.LoadAsync<CarLicense>("ActivateDriver/DeActivate", CustomerId);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("Taxi/")]
        [HttpPost]
        public async Task<IActionResult> PostTaxi([FromBody] CarLicense carlicense )
        {
            
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
                var data = await wac.PostAsync<CarLicense,CarLicense>("CarDetails/Update", carlicense);
                return Ok(data);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }
        [Route("Bus/")]
        [HttpPost]
        public async Task<IActionResult> PostBus([FromBody] CarLicense carlicense)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "busapi-service", AccessToken());
                var data = await wac.PostAsync<CarLicense, CarLicense>("ActivateDriver/Activation", carlicense);
                return Ok(data);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }
        [Route("MiniBus/")]
        [HttpPost]
        public async Task<IActionResult> PostMiniBus([FromBody] CarLicense carlicense)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "minibusapi-service", AccessToken());
                var data = await wac.PostAsync<CarLicense, CarLicense>("ActivateDriver/Activation", carlicense);
                return Ok(data);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }



    }
}