﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.TaxiApi
{

    public class TaxiDriverStatus
    {

        public string CustomerId { get;  set; }

        public string ConnectionId { get;  set; }

        public bool Status { get; protected set; }

        public double? DayOfExpiration { get; set; }

        public string Error { get;  set; }


        public TaxiDriverStatus() { }
        


    }
}
