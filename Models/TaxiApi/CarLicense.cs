﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.TaxiApi
{
    public class CarLicense
    {
        public string Id { get; set; }

        public string LicenseNumber { get; set; }

        public bool IsUpdate { get; set; }
    }
}
