﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.Types;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Messages.Commands.TaxiDriver;
using DuaTaxi.CoreApi.Models;
using DuaTaxi.CoreApi.Models.TaxiApi;
using DuaTaxi.CoreApi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TaxiDriverController : BaseController
    {
        private ITaxiService _taxiService;
        protected IOptions<RestEaseOptions> RestEaseConfig;
        private ILogger<TaxiDriverController> _logger;
        WebApiClient wac;
        public TaxiDriverController(ILogger<TaxiDriverController> _logger, IOptions<RestEaseOptions> RestEaseConfig, IBusPublisher busPublisher, ITaxiService taxiService, ITracer tracer, CheckOptions check) 
            : base(busPublisher, tracer, check)
        {
            _taxiService = taxiService;
            this.RestEaseConfig = RestEaseConfig;
            this._logger = _logger;
        }
     
        // stelnw ena message edw 
        [HttpPost]
        public async Task<IActionResult> Post(CreateTaxiDriver command)
            => await SendAsync(command.BindId(c => c.Id), resourceId: command.Id.ToGuid(), resource: "taxiapi");


        [HttpGet("{Id:guid}")]
        public async Task<ActionResult<object>> Get(Guid Id)
            => await _taxiService.GetAsync(Id);

        [Route("OnInit")]
        [HttpPost]
        public async Task<IActionResult> OnInit([FromBody] InitializeTaxiDriver initializeTaxi)
        {                                                                                
         
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());

                var data = await wac.LoadAsync<TaxiDriverStatus>("InitDriver/GetById", initializeTaxi.CustomerId);
                switch (data.Error) {
                    case "No Payment Exist":
                    return BadRequest(data);
                    case "Driver not exist":
                    return BadRequest(data);
                    case "Payment expired":
                    return BadRequest(data);                   
                    default:
                    break;
                }

                return Ok(data);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }             

        [Route("MapPositions")]
        [HttpPost]
        public async Task<IActionResult> MapPosition([FromBody] TaxiDriverMapPosition activation)
     => await SendAsync(activation.BindId(c => c.Id), resourceId: activation.Id.ToGuid(), resource: "taxiapi");

        [Route("Activation")]
        [HttpPost]
        public async Task<IActionResult> Activation([FromBody] TaxiDriverActivation activation)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());

                var data = await wac.PostAsync< TaxiDriverActivation, ActiveDrivers>("ActivateDriver/Activation", activation);

                if(data is null) {
                    return BadRequest("Driver Activation Error");
                }
                return Ok(data);

            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }

        [Route("DeActivate/{CustomerId}")]
        [HttpGet()]
        public async Task<IActionResult> DeActivate(string CustomerId)
        {
       
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());

                var data = await wac.LoadAsync<ActiveDrivers>("ActivateDriver/DeActivate", CustomerId);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }

        [Route("ReActivate/{CustomerId}")]
        [HttpGet()]
        public async Task<bool> ReActivate(string CustomerId)
        {             
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
                var data = await wac.LoadAsync<bool>("ActivateDriver/ReActivate", CustomerId);

                return data;
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }

        [Route("GetRide/{CustomerId}")]
        [HttpGet()]
        public async Task<IActionResult> GetRideHistory(string CustomerId)
        {
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
                var data = await wac.LoadAsync<bool>("Ride/GetRide", CustomerId);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }

        [Route("CreateNewRide")]
        [HttpPost()]
        public async Task<IActionResult> CreateNewRide([FromBody] TaxiTransportationHistory taxiTransportation)
        {
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
                var data = await wac.PostAsync<TaxiTransportationHistory>("Ride/CreateNewRide", taxiTransportation);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }

        [Route("UpdateRide")]
        [HttpPost()]
        public async Task<IActionResult> UpdateRide(string CustomerId)
        {
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
                var data = await wac.LoadAsync<bool>("Ride/UpdateRide", CustomerId);

                return Ok(data);
            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }
    }
}