﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common;
using DuaTaxi.Common.CustomCheck;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
     
        private readonly IOptions<AppOptions> _options;
      
        public HomeController(IOptions<AppOptions> options, CheckOptions check)
        {
            _options = options;            
        }

        [HttpGet]
        public IActionResult Get() => Ok(_options.Value.Name);
    }
}