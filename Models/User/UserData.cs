﻿using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.User
{
    public class UserData: BaseEntity
    {
    public string Name { get; set; }

    public string Email { get; set; }

    public string PhoneNumber { get; set; }

    

    public UserData() { }

    public UserData(string id, string Name, string Email, string PhoneNumber) : base(id)
    {
        this.Name = Name;
        this.Email = Email;
        this.PhoneNumber = PhoneNumber;       
    }
}
}
