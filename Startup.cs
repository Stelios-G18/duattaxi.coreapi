using System;
using System.Reflection;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Consul;
using DuaTaxi.Common;
using DuaTaxi.Common.Consul;
using DuaTaxi.Common.CustomApiCheck;
using DuaTaxi.Common.Dispatchers;
using DuaTaxi.Common.Jaeger;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.Redis;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.Swagger;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;

namespace DuaTaxi.CoreApi
{
    public class Startup
    {
        private static readonly string[] Headers = new[] { "X-Operation", "X-Resource", "X-Total-Count" };
        public IContainer Container { get; private set; }

        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }



        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //services.Configure<CookiePolicyOptions>(options => {
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;                

            //});

            IdentityModelEventSource.ShowPII = true;
            services.AddCustomMvc();
            services.AddSwaggerDocs();
            services.AddConsul();
            services.AddJaeger();
            services.AddOpenTracing();
            services.AddRedis();


            //    services.AddAuthorization(x => x.AddPolicy("admin", p => p.RequireRole("admin")));
            services.AddCors(options => {
                options.AddPolicy("CorsPolicy", cors =>
                        cors.WithOrigins(Configuration["Cors"])   
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials()
                            .WithExposedHeaders(Headers));
            });
            services.RegisterServiceForwarder<IOperationsService>("operations-service");
            services.RegisterServiceForwarder<ITaxiService>("taxiapi-service");
            //services.RegisterServiceForwarder<IOrdersService>("orders-service");
            //services.RegisterServiceForwarder<IProductsService>("products-service");


            
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })                  
            .AddJwtBearer(o => {
                o.Authority = Configuration["IdentityUrl"];
                o.Audience = "resourceapi";
                o.RequireHttpsMetadata = false;
                o.TokenValidationParameters = new TokenValidationParameters() {
                    ClockSkew = TimeSpan.FromDays(1),
                    // Na kanw mia dokimh me auto.

                    //---- - RoleClaimType = "role"

                };

            });

           
            //services.AddAuthorization(options =>
            //{
            //    options.AddPolicy("ApiReader", policy => policy.RequireClaim("scope", "api.read"));
            //    options.AddPolicy("Consumer", policy => policy.RequireClaim(ClaimTypes.Role, "consumer"));               

            //});


            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.GetEntryAssembly())
                    .AsImplementedInterfaces();
            builder.Populate(services);
            builder.AddDispatchers();
            builder.AddRabbitMq();
            builder.AddCustomerCheck();


            Container = builder.Build();

            return new AutofacServiceProvider(Container);
        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
           IApplicationLifetime applicationLifetime, IConsulClient client,
           IStartupInitializer startupInitializer)
        {
            if (env.IsDevelopment() || env.EnvironmentName == "local") {
                app.UseDeveloperExceptionPage();
            }
            Console.WriteLine("$EnvironmentName "+ env.EnvironmentName);

            app.UseCors("CorsPolicy");
            app.UseAllForwardedHeaders();
            app.UseSwaggerDocs();
            app.UseErrorHandler();            
            // app.UseCors(options => options.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            app.UseAuthentication();
            app.UseServiceId();
            app.UseMvc();
            app.UseRabbitMq();

            var consulServiceId = app.UseConsul();

            applicationLifetime.ApplicationStopped.Register(() => {
                client.Agent.ServiceDeregister(consulServiceId);
                Container.Dispose();
            });

            startupInitializer.InitializeAsync();
        }

    }
}
