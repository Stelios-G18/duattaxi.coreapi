﻿using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.TaxiApi
{
     public class Customer : BaseEntity
        {
            public string Name { get; set; }

            public string Email { get; set; }

            public string PhoneNumber { get; set; }

            public DriverTypes Type { get; set; }

            public string CarPlate { get; set; }

            public Customer(){}

            public Customer(string id, string Name, string Email, string PhoneNumber, DriverTypes types, string CarPlate) : base(id)
            {

                this.Name = Name;
                this.Email = Email;
                this.PhoneNumber = PhoneNumber;
                this.Type = types;
                this.CarPlate = CarPlate;
            }
        }
    
}
