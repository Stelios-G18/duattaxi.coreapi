﻿using System;
using System.Threading.Tasks;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Models.Payment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers                                    
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PaymentController : BaseController
    {
        protected IOptions<RestEaseOptions> RestEaseConfig;
        private ILogger<PaymentController> _logger;
        WebApiClient wac;
        public PaymentController(ILogger<PaymentController> logger, IOptions<RestEaseOptions> RestEaseConfig, IBusPublisher busPublisher, ITracer tracer, CheckOptions check) : base(busPublisher, tracer, check)
        {
            this._logger = logger;
            this.RestEaseConfig = RestEaseConfig;
        }


        [Route("Status/{CustomerId}")]
        [HttpGet]
        public async Task<IActionResult> OnInit(string CustomerId)
        {
           
            try {
                wac = new WebApiClient(RestEaseConfig, "payment-service", AccessToken());

                var payment = await wac.LoadAsync<PaymentDto>("Payment/GetByCustomerId", CustomerId);

                return Ok(payment);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("Create")]
        [HttpPost]
        public async Task<IActionResult> Create(PaymentDto payment)
        {          
            try {
                wac = new WebApiClient(RestEaseConfig, "payment-service", AccessToken());

                var data = await wac.PostAsync<PaymentDto, PaymentDto>("Payment/Create", payment);

                return Ok(payment);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

    }
}