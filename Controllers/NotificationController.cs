﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Models.Notification;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class NotificationController : BaseController
    {
        protected IOptions<RestEaseOptions> RestEaseConfig;
        private ILogger<PaymentController> _logger;
        WebApiClient wac;
        public NotificationController(ILogger<PaymentController> _logger, IOptions<RestEaseOptions> RestEaseConfig, IBusPublisher busPublisher, ITracer tracer, CheckOptions check) 
            : base(busPublisher, tracer, check)
        {
            this._logger = _logger;
            this.RestEaseConfig = RestEaseConfig;
        }


        [Route("Count/{CustomerId}")]
        [HttpGet]
        public async Task<IActionResult> Count(string CustomerId)
        {  
            try {
                wac = new WebApiClient(RestEaseConfig, "signalr-service", AccessToken());
                var payment = await wac.LoadAsync<long>("Notification/Count", CustomerId);

                return Ok(payment);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("Data/{CustomerId}")]
        [HttpGet]
        public async Task<IActionResult> Data(string CustomerId)
        {
         
            try {
                wac = new WebApiClient(RestEaseConfig, "signalr-service", AccessToken());

                var payment = await wac.LoadAsync<IEnumerable<NotificationDTO>>("Notification/Data", CustomerId);

                return Ok(payment);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("Update/{CustomerId}")]
        [HttpGet]
        public async Task<IActionResult> Update(string CustomerId)
        {
          
            try {
                wac = new WebApiClient(RestEaseConfig, "signalr-service", AccessToken());

                var payment = await wac.LoadAsync<bool>("Notification/Update", CustomerId);

                return Ok(payment);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

    }
}