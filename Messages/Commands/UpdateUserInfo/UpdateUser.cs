﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Messages.Commands.UpdateUserInfo
{
    [MessageNamespace("authserver")]
    public class UpdateUser : ICommand, IIdentifiable
    {
        public string Id { get; set; }

        public string Name { get; set; }


        public string Email { get; set; }


        public string PhoneNumber { get; set; }


        public string CarPlate { get; set; }

    }
}
