﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Models.TaxiApi;
using DuaTaxi.CoreApi.Models.TaxiSetPick;
using DuaTaxi.CoreApi.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ClientController : BaseController
    {
        protected IOptions<RestEaseOptions> RestEaseConfig;
        private ILogger<TaxiDriverController> _logger;
        WebApiClient wac;
        public ClientController(ILogger<TaxiDriverController> _logger, IOptions<RestEaseOptions> RestEaseConfig, IBusPublisher busPublisher, ITracer tracer, CheckOptions check) : base(busPublisher, tracer, check)
        {
            this.RestEaseConfig = RestEaseConfig;
            this._logger = _logger;
        }       

        [Route("Taxi/SetPickup")]
        [HttpPost]
        public async Task<IActionResult> OnInit([FromBody] SetPickup setpickup)
        {
           
            try {
                wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
                var data = await wac.PostAsync<SetPickup, ClosesActiveDriversDTO>("TaxiDriver/SetPickup",setpickup);
                return Ok(data);

            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }            

        }


        [Route("Taxi/CreateNewRide")]
        [HttpPost]
        public async Task<IActionResult> CreateNewRide([FromBody] UserTransportationHistory ride)
        {
            try {
                wac = new WebApiClient(RestEaseConfig, "userapi-service", AccessToken());
                var data = await wac.PostAsync<UserTransportationHistory>("Ride/Create", ride);
                return Ok();

            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }


        [Route("Taxi/UpdateRide")]
        [HttpPost]
        public async Task<IActionResult> UpdateRide([FromBody] UserTransportationHistory ride)
        {
            try {
                wac = new WebApiClient(RestEaseConfig, "userapi-service", AccessToken());
                var data = await wac.PostAsync<UserTransportationHistory>("Ride/Update", ride);
                return Ok();

            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }
        }
    }
}