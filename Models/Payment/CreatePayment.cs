﻿using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.Payment
{
    public class CreatePayment
    {
        public string CustomerId { get; set; }  

        public DriverTypes Type { get; set; }

        public decimal Amount { get; set; }

        public PaymentTypes PaymentType { get; set; }   

    }
}
