#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.
#  cd ..     docker build -f .\DuaTaxi.CoreApi\Dockerfile -t  stelgio/duataxi:v3 .
#            docker run -p 5555:5555 --network=duataxi-network  stelgio/duataxi:v3

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine  AS base
WORKDIR /app
ENV ASPNETCORE_URLS http://*:5555
ENV ASPNETCORE_ENVIRONMENT docker
EXPOSE 5555


FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine AS build
WORKDIR /src
COPY ["./DuaTaxi.CoreApi/DuaTaxi.CoreApi.csproj", "DuaTaxi.CoreApi/"]
#COPY . "DuaTaxi.CoreApi/"
RUN dotnet restore "./DuaTaxi.CoreApi/DuaTaxi.CoreApi.csproj"
COPY . .
WORKDIR "/src/DuaTaxi.CoreApi"
RUN dotnet build "DuaTaxi.CoreApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "DuaTaxi.CoreApi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DuaTaxi.CoreApi.dll"]



#FROM mcr.microsoft.com/dotnet/core/sdk:2.2-alpine as build
#WORKDIR /app
#
#COPY ./DuaTaxi.CoreApi/*.csproj .
#RUN dotnet restore -r linux-musl-x64
#
#COPY . .
#RUN dotnet publish -c Release -r linux-musl-x64 -o out --no-restore
#
#FROM mcr.microsoft.com/dotnet/core/runtime-deps:2.2-alpine as runtime
#WORKDIR /app
#ENV ASPNETCORE_URLS http://*:5555
#ENV ASPNETCORE_ENVIRONMENT docker
#EXPOSE 5555
#COPY --from=build /app/out ./
#ENTRYPOINT ["./"]