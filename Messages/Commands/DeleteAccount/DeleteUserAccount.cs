﻿using DuaTaxi.Common.Messages;
using DuaTaxi.Common.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Messages.Commands.DeleteAccount
{
    [MessageNamespace("authserver")]
    public class DeleteUserAccount : ICommand , IIdentifiable
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
