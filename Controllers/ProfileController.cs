﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DuaTaxi.Common;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.Common.RestEase;
using DuaTaxi.Common.WebApiClient;
using DuaTaxi.CoreApi.Messages.Commands.DeleteAccount;
using DuaTaxi.CoreApi.Messages.Commands.UpdatePassword;
using DuaTaxi.CoreApi.Messages.Commands.UpdateUserInfo;
using DuaTaxi.CoreApi.Models.TaxiApi;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProfileController : BaseController
    {

        protected IOptions<RestEaseOptions> RestEaseConfig;
        private ILogger<ProfileController> _logger;
        WebApiClient wac;
        public ProfileController(ILogger<ProfileController> _logger, IOptions<RestEaseOptions> RestEaseConfig, IBusPublisher busPublisher, ITracer tracer, CheckOptions check) : base(busPublisher, tracer, check)
        {               
            this._logger = _logger;
            this.RestEaseConfig = RestEaseConfig;
        }

        [Route("TaxiProfileData/{CustomerId}")]
        [HttpGet]
        public async Task<IActionResult> OnInit(string CustomerId)
        {
            wac = new WebApiClient(RestEaseConfig, "taxiapi-service", AccessToken());
            try {
                

                var data = await wac.LoadAsync<Customer>("Customer/GetById", CustomerId);

                return Ok(data);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("DriverProfileData/UpdateUserInfo")]
        [HttpPost]
        public async Task<IActionResult> Post(UpdateUser command)
          => await SendAsync(command, resourceId: command.Id.ToGuid(), resource: "authserver");

        [Route("DriverProfileData/UpdateUserPassword")]
        [HttpPost]
        public async Task<IActionResult> Post(UpdateUserPassword command)
          => await SendAsync(command, resourceId: command.Id.ToGuid(), resource: "authserver");

        [Route("DriverProfileData/delete")]
        [HttpPost]
        public async Task<IActionResult> Post(DeleteUserAccount command)
        => await SendAsync(command, resourceId: command.Id.ToGuid(), resource: "authserver");



        //USER PROFILE END POINTS



        [Route("UserProfileData/{UserId}")]
        [HttpGet]
        public async Task<IActionResult> GetUserData(string UserId)
        {
            wac = new WebApiClient(RestEaseConfig, "authapi-service", AccessToken());
            try {


                var data = await wac.LoadAsync<Customer>("Account/GetById", UserId);

                return Ok(data);


            } catch (Exception ex) {
                _logger.LogError(ex.Message, ex.InnerException, ex.StackTrace);
                throw new Exception(ex.Message, ex);
            }

        }

        [Route("UserProfileData/UpdateUserInfo")]
        [HttpPost]
        public async Task<IActionResult> UpdateUserInfo(UpdateUser command)
         => await SendAsync(command, resourceId: command.Id.ToGuid(), resource: "authserver");

        [Route("UserProfileData/UpdateUserPassword")]
        [HttpPost]
        public async Task<IActionResult> UpdateUserPassword(UpdateUserPassword command)
          => await SendAsync(command, resourceId: command.Id.ToGuid(), resource: "authserver");

        [Route("UserProfileData/delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(DeleteUserAccount command)
        => await SendAsync(command, resourceId: command.Id.ToGuid(), resource: "authserver");


    }
}