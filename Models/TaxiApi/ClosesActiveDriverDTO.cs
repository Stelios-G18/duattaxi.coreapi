﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DuaTaxi.CoreApi.Models.TaxiApi
{
    public class ClosesActiveDriversDTO
    {
        public string Id { get; set; }
        public double Distance { get; set; }
        public double? Lat { get; set; }
        public double? Long { get; set; }
        public string DriverPhoneNumber { get; set; }
        public string DriverName { get; set; }
        public string ConnectionId { get; set; }
        public string CarPlate { get; set; }

        public string Error { get; set; }

        public ClosesActiveDriversDTO()
        {

        }
    }
}
