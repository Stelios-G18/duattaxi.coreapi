﻿using System.Threading.Tasks;
using DuaTaxi.Common;
using DuaTaxi.Common.CustomCheck;
using DuaTaxi.Common.Mvc;
using DuaTaxi.Common.RabbitMq;
using DuaTaxi.CoreApi.Messages.Commands.SMTP;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenTracing;

namespace DuaTaxi.CoreApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactController : BaseController
    {
        private ILogger<ContactController> _logger;

        public ContactController(ILogger<ContactController> _logger,IBusPublisher busPublisher,  ITracer tracer, CheckOptions check) : base(busPublisher, tracer, check)
        {
            this._logger = _logger;
        }

        [HttpPost]       
        public async Task<IActionResult> Post([FromBody]Contact command)
          => await SendAsync(command.BindId(c => c.Id), resourceId: command.Id.ToGuid(), resource: "smtpApi");
    }
}